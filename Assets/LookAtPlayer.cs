﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class LookAtPlayer : MonoBehaviour
{

	public Transform target;
	public Transform forward;
	public float maxDistanceToTarget;
	public bool onlyWhenRaycast;
	public LayerMask wallsLayerMask;

	// Use this for initialization
	void Start()
	{
		target = GameObject.Find("CenterEyeAnchor").transform;
	}

	// Update is called once per frame
	void Update()
	{
		if (target != null && target.gameObject.activeInHierarchy) {
			LookAt(target.position);
		}
	}

	public float smoothAlpha = 0.1f;


	void LookAt(Vector3 pos)
	{
		//var rayStart = target.position + Vector3.up * 2;
		//var rayEnd = target.position;
		//var distTo = Mathf.Clamp((rayEnd - rayStart).magnitude, 0, maxDistanceToTarget);
		//var ra = Physics.RaycastAll(rayStart, rayEnd, distTo, wallsLayerMask);
		//var wallInTheWay = ra.Where(rh => (rh.point - rayStart).sqrMagnitude < distTo * distTo).Any();

		//var raycastCondition = (onlyWhenRaycast && !wallInTheWay) || !onlyWhenRaycast;
		var raycastCondition = true;

		// lookat smooth
		if (raycastCondition && (maxDistanceToTarget == 0 || (target.position - transform.position).sqrMagnitude <= maxDistanceToTarget * maxDistanceToTarget)) {
			var initRot = transform.rotation;
			transform.LookAt(pos);
			var targetrot = transform.rotation;
			transform.rotation = initRot;
			transform.rotation = Quaternion.Lerp(initRot, targetrot, smoothAlpha * Time.deltaTime);
		} else {
			// look forward
			var targetrot = forward.rotation;
			transform.rotation = Quaternion.Lerp(transform.rotation, targetrot, smoothAlpha * Time.deltaTime);
		}


	}
}
