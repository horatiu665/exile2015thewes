﻿using UnityEngine;
using System.Collections;

public class RaiseFogWhenMove : MonoBehaviour
{

	public float targetValue = 1.99f;
	public float dur;
	public Light dimThis;
	public Light doorLight;
	public float doorLightTargetRange;
	void Start()
	{

	}

	void Update()
	{
		var notMoving = Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0;
		if (!notMoving) {
			var c = GetComponent<UnityStandardAssets.ImageEffects.GlobalFog>();
			float initInt = 0;
			float initr = 0;
			if (dimThis != null) {
				initInt = dimThis.intensity;
			}
			if (doorLight != null) {
				initr = doorLight.range;
			}
			c.StartCoroutine(pTween.To(dur, t => {
				if (dimThis != null) {
					dimThis.intensity = (1 - t) * initInt;
				}
				if (doorLight != null) {
					doorLight.range = Mathf.Lerp(initr, doorLightTargetRange, t);
				}
				c.height = t * targetValue;
			}));
			Destroy(this);
		}
	}
}
