﻿using UnityEngine;
using System.Collections;

public class RandomizePitch : MonoBehaviour {

	public Vector2 pitch;
	// Use this for initialization
	void Start () {
		GetComponent<AudioSource>().pitch = Random.Range(pitch.x, pitch.y);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
