﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class Spawner : MonoBehaviour
{
	public Transform[] spawnWhat;
	public int howMany = 1;
	public float spawnRadius = 10;
	[Range(1, 1.999f)]
	public float variousSizes = 1;
	public float minDistanceToOthers = 0;
	public bool distributedRandom = true;
	public bool randomize;
	public int randomSeed = 0;
	public float delay = 0;
	public Color editorColor;
	public bool showGizmos = false;
	public Transform parentTo;
	public bool spawnOnStart = false;
	public bool doItNow = false;
	public bool clearChildren = false;
	public bool afterMove = true;

	void Update()
	{
		if (doItNow) {
			doItNow = false;
			StartCoroutine(Spawn());
		}

		if (clearChildren) {
			clearChildren = false;
			while (transform.childCount > 0) {
				DestroyImmediate(transform.GetChild(transform.childCount - 1).gameObject);
			}
		}

	}

	void Randomize(int s)
	{
		Random.seed = s;
	}

	IEnumerator ListenForMove()
	{
		while (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0) {
			yield return 0;
		}

		StartCoroutine(Spawn());
		
	}

	void Start()
	{
		if (Application.isPlaying) {
			if (spawnOnStart) {
				StartCoroutine(Spawn());

			}
			if (afterMove) {
				StartCoroutine(ListenForMove());
			}
		}
	}

	IEnumerator Spawn()
	{

		if (delay > 0) {
			yield return new WaitForSeconds(delay);
		}

		if (randomize)
			Randomize(randomSeed);

		// spawner
		for (int i = 0; i < howMany; i++) {
			// little more normally distributed random position
			Vector3 pos;

			// count how many times we attempt to set position (Monte Carlo anyone?)
			int iterations = 0;
			// set pos
			do {
				iterations++;
				pos = distributedRandom
					? (Random.insideUnitCircle + Random.insideUnitCircle) / 2 * spawnRadius
					: Random.insideUnitCircle * spawnRadius;
				pos = new Vector3(transform.position.x + pos.x, transform.position.y, transform.position.z + pos.y);
			} while (minDistanceToOthers != 0 && !PositionFarFromAll(pos, minDistanceToOthers) && iterations < 50);

			Transform a = Instantiate(spawnWhat[Random.Range(0, spawnWhat.Length)], pos, Quaternion.identity) as Transform;
			a.localScale *= Random.Range(2 - variousSizes, variousSizes);
			if (parentTo != null) {
				a.parent = parentTo;
			}
		}

	}

	bool PositionFarFromAll(Vector3 pos, float minDistance)
	{
		// get all children
		List<Transform> children = new List<Transform>();
		for (int i = 0; i < transform.childCount; i++) {
			children.Add(transform.GetChild(i));
		}

		// check distance to each child. if one of them is out of bounds, return false
		foreach (var c in children) {
			if ((c.position - pos).sqrMagnitude < minDistance * minDistance) {
				return false;
			}
		}

		return true;
	}

	void OnDrawGizmos()
	{
		if (showGizmos) {
			Gizmos.color = editorColor;
			Gizmos.DrawWireSphere(transform.position, spawnRadius);
		}
	}

	/// <summary>
	/// could be used for finding unique positions for each element, so they are not overlapping terribly. instead, a dirty fix can also be found.
	/// </summary>
	float radiusOfObjects(int numObjects, float circleRadius)
	{
		return circleRadius / Mathf.Sqrt(numObjects);

	}

}
