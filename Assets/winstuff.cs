﻿using UnityEngine;
using System.Collections;

public class winstuff : MonoBehaviour {


	void OnTriggerEnter(Collider c)
	{
		if (c.tag != "Player") return;

		WinCondition.inst.LoseGame();
	}


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {

			WinCondition.inst.LoseGame();
			Destroy(this);
		}
	}
}
