﻿using UnityEngine;
using System.Collections;

public class CountAgents : MonoBehaviour {

	public static int count;
	public static bool initialized;
	public static CountAgents inst;
	public int maxAgents;

	// Use this for initialization
	void Awake () {
		inst = this;
		if (!initialized) {
			initialized = true;
			count = 0;
		}
		count++;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
