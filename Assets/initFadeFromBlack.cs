﻿using UnityEngine;
using System.Collections;

public class initFadeFromBlack : MonoBehaviour
{

	public Renderer whitePix;
	public float alpha;
	public float dur;
	public bool destroy = true;

	public AnimationCurve fadeCurve;

	public bool onStart = true;

	void Update()
	{
		whitePix.material.color = new Color(whitePix.material.color.r, whitePix.material.color.g, whitePix.material.color.b, alpha);

	}

	void Start()
	{
		if (!onStart) return;
		FadeIt();

	}

	public void FadeIt() {
		StartCoroutine(pTween.To(dur, t => {
			alpha = fadeCurve.Evaluate(1 - t);
			if (t == 1) {
				if (destroy) {
					Destroy(whitePix.gameObject);
					Destroy(gameObject);
				}
			}
		}));
	}



}
