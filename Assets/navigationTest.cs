﻿using UnityEngine;
using System.Collections;

public class navigationTest : MonoBehaviour {

	public Transform target;

	// Use this for initialization
	void Start () {
		InvokeRepeating("Update2222", 0, 0.5f);
	}
	
	// Update is called once per frame
	void Update2222 () {
		var nav = GetComponent<NavMeshAgent>();
		nav.SetDestination(target.position);
		var rm = GetComponent<RotationModule>();
		rm.RotateTowards(nav.nextPosition);
	}
}
