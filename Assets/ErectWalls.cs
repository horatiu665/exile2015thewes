﻿using UnityEngine;
using System.Collections;

public class ErectWalls : MonoBehaviour
{

	public AnimationCurve curve;
	public float erectionDuration;
	public float height;
	bool erected;

	public AudioSource[] wallSounds;
	public AudioSource wallLoopSound;
	public AnimationCurve wallLoopVol;
	public float wallLoopVolDuration;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (erected) return;
		if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
			erected = true;
			Erect();
		}
	}

	void Erect()
	{
		var ini = transform.position;
		StartCoroutine(pTween.To(erectionDuration, t => {
			transform.position = Vector3.up * curve.Evaluate(t) * height + ini;
		}));
		foreach (var wallSound in wallSounds) {
			wallSound.Play();

		}
		wallLoopSound.Play();
		StartCoroutine(pTween.To(wallLoopVolDuration, t => {
			wallLoopSound.volume = wallLoopVol.Evaluate(t);
		}));
	}
}
