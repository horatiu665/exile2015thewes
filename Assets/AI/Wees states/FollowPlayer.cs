﻿using UnityEngine;
using System.Collections;
using PlantmanAI4;
using System.Linq;

public class FollowPlayer : MonoBehaviour, IState
{

	public Transform target;
	public Transform altTarget;
	private Transform agent;
	private NavMeshAgent navMeshAgent;

	public float priority = 0f;

	public float minDistToPlayer = 0f;
	public float maxDistToPlayer = 0;

	public Vector2 timeToWaitBeforeFollow;
	float waitTimer = 0;

	public bool onlyWhenRaycast;

	public LayerMask wallsLayerMask;



	void Start()
	{
		agent = transform.parent;
		navMeshAgent = agent.GetComponent<NavMeshAgent>();

		if (target == null || !target.gameObject.activeInHierarchy)
			target = altTarget;

		if (target == null) {
			target = GameObject.Find("CenterEyeAnchor").transform;
		}

	}

	public string GetName()
	{
		return "Follow Player";
	}

	public bool GetUninterruptible()
	{
		return false;
	}

	public float GetPriority()
	{
		return priority;
	}

	public void OnEnter()
	{
		waitTimer = Time.time + Random.Range(timeToWaitBeforeFollow.x, timeToWaitBeforeFollow.y);
		navMeshAgent.SetDestination(transform.position);
	}

	public void OnExecute(float deltaTime)
	{

		if (Time.time > waitTimer) {
			var sqrM = (target.position - agent.position).sqrMagnitude;
			if (sqrM > minDistToPlayer * minDistToPlayer) {
				//Vector3 directionTowardsRight = Vector3.zero;
				// direction towards target
				Vector3 dir = target.position - transform.position;

				// distance to target
				var distToTarget = dir.magnitude;

				var navMeshTarget = target.position;

				navMeshAgent.SetDestination(navMeshTarget);
			} else {

				navMeshAgent.SetDestination(transform.position);
			}
		}
	}

	public void OnExit()
	{

	}
	public float raycastHeight = 2f;
	public bool ConditionsMet()
	{
		// dist to player within limit
		if (agent == null) return false;
		if (target == null) return false;

		var sqrM = (agent.position - target.position).sqrMagnitude;
		
		//var rayStart = agent.position + Vector3.up * raycastHeight;
		//var rayEnd = target.position;
		//var raycastCondition = (onlyWhenRaycast && !Physics.Raycast(rayStart, rayEnd, Mathf.Clamp((rayEnd - rayStart).magnitude, minDistToPlayer, maxDistToPlayer), wallsLayerMask)) || !onlyWhenRaycast;

		//var rayStart = agent.position + Vector3.up * 2;
		//var rayEnd = agent.position;
		//var distToTarget = (rayEnd - rayStart).magnitude;
		//var ra = Physics.RaycastAll(rayStart, rayEnd, Mathf.Clamp(distToTarget, 0, maxDistToPlayer), wallsLayerMask);
		//var wallInTheWay = ra.Where(rh => (rh.point - rayStart).sqrMagnitude < distToTarget * distToTarget).Any();

		//var raycastCondition = (onlyWhenRaycast && !wallInTheWay) || !onlyWhenRaycast;
		var raycastCondition = true;
		//print("See you " + (sqrM < maxDistToPlayer * maxDistToPlayer));
		var cond = (raycastCondition && sqrM < maxDistToPlayer*maxDistToPlayer);
		Debug.DrawLine(agent.position + Vector3.up * raycastHeight, target.position, cond ? Color.green : Color.red);
		return cond;
	}
}



/*

Here is a basic starting point for making new states:

using UnityEngine;
using System.Collections;
using PlantmanAI4;


public class FollowPlayer : MonoBehaviour, IState
{

	private Transform agent;

	void Start()
	{
		agent = transform.parent;

	}

	public string GetName()
	{
		return "Follow Player";
	}

	public bool GetUninterruptible()
	{
		return false;
	}

	public float GetPriority()
	{
		return 0;
	}

	public void OnEnter()
	{
		
	}

	public void OnExecute(float deltaTime)
	{

	}

	public void OnExit()
	{

	}

	public bool ConditionsMet()
	{
		return true;
	}
}


*/