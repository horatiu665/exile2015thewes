﻿using UnityEngine;
using System.Collections;
using PlantmanAI4;

public class Multiply : MonoBehaviour, IState
{

	private Transform agent;
	private NavMeshAgent navMeshAgent;

	public float priority;

	public float randomChancePerSecond;

	bool readyToReproduce = false;
	bool reproducing = false;
	public AudioSource reproduceSound;

	public bool onlyOnce;

	public float reproduceDuration;
	public AnimationCurve reproduceMovement;
	public Vector3 reproduceMovementDir;

	public Transform duplicate;

	void Start()
	{
		agent = transform.parent;
		navMeshAgent = agent.GetComponent<NavMeshAgent>();

		Invoke("AttemptToReproduce", Random.Range(0, 1f));
	}

	void AttemptToReproduce()
	{
		if (!reproducing) {
			var ch = Random.Range(0, 100f);
			if (ch < randomChancePerSecond) {
				// reproduce!!!
				readyToReproduce = true;
			}
		}

		Invoke("AttemptToReproduce", 1f);
	}

	public string GetName()
	{
		return "Multiply";
	}

	public bool GetUninterruptible()
	{
		return reproducing;
	}

	public float GetPriority()
	{
		return priority;
	}

	public void OnEnter()
	{
		//var newAsss = Instantiate(agent, agent.position, agent.rotation) as Transform;
		//reproducing = false;




		//return;



		var nav = transform.parent.GetComponent<NavMeshAgent>();
		//nav.enabled = false;
		nav.SetDestination(transform.position);

		readyToReproduce = false;
		var init = agent.position;
		reproducing = true;
		var anim = agent.GetComponent<Animator>();
		anim.SetBool("Reproducing", true);
		//anim.playbackTime = Random.Range(0f, anim.GetCurrentAnimatorStateInfo(0).length);


		// create other agent
		Transform newA;
		if (duplicate != null) {
			newA = Instantiate(duplicate, agent.position, agent.rotation) as Transform;
		} else {
			newA = Instantiate(agent, agent.position, agent.rotation) as Transform;
		}
		newA.localScale = Random.Range(0.9f, 1.1f) * Vector3.one;

		// play  sound
		if (reproduceSound != null) {
			reproduceSound.Play();
		}

		var anim2 = newA.GetComponent<Animator>();
		anim2.SetBool("Reproducing", true);
		//anim2.playbackTime = Random.Range(0f, anim2.GetCurrentAnimatorStateInfo(0).length);

		// move dudes
		StartCoroutine(pTween.To(reproduceDuration, t => {
			agent.position = init + agent.TransformDirection(reproduceMovementDir) * reproduceMovement.Evaluate(t);
			newA.position = init - agent.TransformDirection(reproduceMovementDir) * reproduceMovement.Evaluate(t);
			if (t == 1) {
				reproducing = false;
				anim.SetBool("Reproducing", false);
				anim2.SetBool("Reproducing", false);
				nav.enabled = true;
			}
		}));


	}

	public void OnExecute(float deltaTime)
	{

	}

	public void OnExit()
	{
		if (onlyOnce) {
			Destroy(this);
		}
	}

	public bool ConditionsMet()
	{
		if (CountAgents.count > CountAgents.inst.maxAgents) return false;
		return readyToReproduce;
	}
}
