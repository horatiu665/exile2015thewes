﻿using UnityEngine;
using System.Collections;

public class NodeGizmo : MonoBehaviour {

	public float size = 0.2f;
	public Color gizmosColor;

	void OnDrawGizmos()
	{
			Gizmos.color = gizmosColor;
			Gizmos.DrawSphere(transform.position, size);
	}
}
