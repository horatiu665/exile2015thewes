﻿
using UnityEngine;
using System.Collections;
using PlantmanAI4;


public class WanderHome : MonoBehaviour, IState
{

	private Transform agent;
	private NavMeshAgent navMeshAgent;

	public Transform homeTransform;
	public LayerMask homeLayerMask;
	public float rangeInHome = 50f;
	public float priority;

	void Start()
	{
		agent = transform.parent;
		navMeshAgent = agent.GetComponent<NavMeshAgent>();

	}

	public string GetName()
	{
		return "Wander Home";
	}

	public bool GetUninterruptible()
	{
		return false;
	}

	public float GetPriority()
	{
		return priority;
	}

	public void OnEnter()
	{
		// if not home, send home.
		if (!IsInsideCollider(agent.position, homeLayerMask)) {
			SendHome();

		} else {
			Wander();

		}
	}

	void SendHome()
	{
		// set target to point within home collider.
		var closestPointHome = homeTransform.GetComponent<Collider>().ClosestPointOnBounds(agent.position);
		Vector3 pointInHome = Vector3.Lerp(closestPointHome, homeTransform.position, 0.5f);

		navMeshAgent.SetDestination(pointInHome);
	}

	void Wander()
	{
		// get point randomly inside home
		Vector3 pointInHome;
		do {
			pointInHome = homeTransform.position + Random.insideUnitSphere * rangeInHome;
		} while (!IsInsideCollider(pointInHome, homeLayerMask));

		// set destination y position on navmesh.
		pointInHome.y = 0;
		NavMeshHit nmh;
		if (NavMesh.SamplePosition(pointInHome, out nmh, 10, navMeshAgent.areaMask)) {
			navMeshAgent.SetDestination(nmh.position);
		}

	}

	public void OnExecute(float deltaTime)
	{
		// if destination reached, wander more
		if (AgentReachedDestination(navMeshAgent)) {
			Wander();
		}

	}

	private bool AgentReachedDestination(NavMeshAgent navMeshAgent)
	{
		if (!navMeshAgent.pathPending) {
			if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) {
				if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f) {
					return true;
				}
			}
		}
		return false;
	}

	public void OnExit()
	{

	}

	public bool ConditionsMet()
	{
		return true;
	}

	bool JotunIsHome()
	{
		return IsInsideCollider(agent.position, homeLayerMask);
	}

	void OnDrawGizmosSelected()
	{
		if (navMeshAgent != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(navMeshAgent.destination, 1);
		}

	}

	/// <summary>
	/// taken and adapted from http://answers.unity3d.com/questions/163864/test-if-point-is-in-collider.html
	/// checks if the transform is within any colliders in a layer within "collisionLayer" by counting how many times any colliders are hit with raycasts from a totally random point in space.
	/// </summary>
	bool IsInsideCollider(Vector3 point, LayerMask collisionLayer)
	{
		if (homeTransform == null) {
			return true;
		}

		Vector3 Checkpoint;
		Vector3 Start = point + new Vector3(0, 3000, 0); // This is defined to be some arbitrary point far away from the collider.
		Vector3 Goal = point; // This is the point we want to determine whether or not is inside or outside the collider.
		Vector3 Direction = Goal - Start; // This is the direction from start to goal.
		Direction.Normalize();
		int Iterations = 0; // If we know how many times the raycast has hit faces on its way to the target and back, we can tell through logic whether or not it is inside.
		Checkpoint = Start;

		while (Checkpoint != Goal) // Try to reach the point starting from the far off point.  This will pass through faces to reach its objective.
        {
			RaycastHit hit;
			if (Physics.Linecast(Checkpoint, Goal, out hit, collisionLayer)) // Progressively move the point forward, stopping everytime we see a new plane in the way.
             {
				Iterations++;
				Checkpoint = hit.point + (Direction / 100.0f); // Move the Point to hit.point and push it forward just a touch to move it through the skin of the mesh (if you don't push it, it will read that same point indefinately).
			} else {
				Checkpoint = Goal; // If there is no obstruction to our goal, then we can reach it in one step.
			}
		}
		while (Checkpoint != Start) // Try to return to where we came from, this will make sure we see all the back faces too.
         {
			RaycastHit hit;
			if (Physics.Linecast(Checkpoint, Start, out hit, collisionLayer)) {
				Iterations++;
				Checkpoint = hit.point + (-Direction / 100.0f);
			} else {
				Checkpoint = Start;
			}
		}
		if (Iterations % 2 == 1) {
			return true;
		}
		return false;
	}
}

