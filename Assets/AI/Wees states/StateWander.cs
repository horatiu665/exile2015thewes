﻿using UnityEngine;
using System.Collections;
using PlantmanAI4;

public class StateWander : MonoBehaviour, IState
{
	private Transform agent;
	private NavMeshAgent navMeshAgent;

	public float priority = 0f;

	public float searchRadius;

	void Start()
	{
		agent = transform.parent;
		navMeshAgent = agent.GetComponent<NavMeshAgent>();

	}

	public string GetName()
	{
		return "Wander";
	}

	public bool GetUninterruptible()
	{
		return false;
	}

	public float GetPriority()
	{
		return priority;
	}

	public void OnEnter()
	{
		Wander();
	}

	public void OnExecute(float deltaTime)
	{
		// if not going anywhere sensible, abandon and choose another direction
		// if destination reached, wander more
		if (AgentReachedDestination(navMeshAgent)) {
			Wander();
		}

	}

	private bool AgentReachedDestination(NavMeshAgent navMeshAgent)
	{
		if (!navMeshAgent.pathPending) {
			if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) {
				if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f) {
					return true;
				}
			}
		}
		return false;
	}

	public void OnExit()
	{
	}

	public bool ConditionsMet()
	{
		return true;
	}


	void Wander()
	{
		if (agent == null) return;
		Vector3 pointAroundAgent;
		pointAroundAgent = agent.position + Random.insideUnitSphere * searchRadius;

		// set destination y position on navmesh.
		pointAroundAgent.y = 0;
		NavMeshHit nmh;
		if (NavMesh.SamplePosition(pointAroundAgent, out nmh, 10, navMeshAgent.areaMask)) {
			navMeshAgent.SetDestination(nmh.position);
		}

	}

}
