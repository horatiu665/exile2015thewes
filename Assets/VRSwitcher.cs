﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VRSwitcher : MonoBehaviour
{
	public static VRSwitcher instance;

	public bool VR = false;
	private bool isVr = false;

	public GameObject oculus, normalCamera;

	void Awake()
	{
		instance = this;
	}

	public void SetVr(bool vr)
	{
		// activate oculus, disable normal camera
		if (oculus != null) {
			oculus.SetActive(vr);
		}
		if (normalCamera != null) {
			normalCamera.SetActive(!vr);
		}

	}

	// Update is called once per frame
	void Update()
	{
		if (Application.isEditor) {
			if (VR != isVr) {
				isVr = VR;
				SetVr(VR);
			}
		} else {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				SetVr(!isVr);
			}
		}
	}
}
