﻿using UnityEngine;
using System.Collections;

public class WeeAnimationSounds : MonoBehaviour {

	public AudioClip[] footsteps;
	public AudioSource footstepSource;

	public AudioClip[] cracks;
	public AudioSource crackSource;

	public bool mute;

	public void PlayFootstep()
	{
		if (mute) return;
		var i = Random.Range(0, footsteps.Length);
		footstepSource.PlayOneShot(footsteps[i]);
	}

	public void PlayCracks()
	{
		if (mute) return;
		if (cracks.Length == 0) return;
		var i = Random.Range(0, cracks.Length);
		crackSource.PlayOneShot(cracks[i]);
	}
}
