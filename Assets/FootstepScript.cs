﻿using UnityEngine;
using System.Collections;

public class FootstepScript : MonoBehaviour {

	public AudioClip[] footsteps;
	public AudioSource audioSource;
	int lastStep;

	Vector3 lastPos;
	float _t;
	public float timeToStep;
	public Vector2 pitch;

	void Start()
	{

		lastPos = transform.position;
	}
	

	void Update () {
		if (IsMoving()) {
			_t += Time.deltaTime;
		}
		if (_t > timeToStep) {
			_t = 0;

			PlayFootstep();
		}

		lastPos = transform.position;
	}


	bool IsMoving()
	{
		return transform.position != lastPos;
	}

	void PlayFootstep()
	{
		var rand = Random.Range(0, footsteps.Length);
		while (footsteps.Length > 1 && rand == lastStep) {
			rand = Random.Range(0, footsteps.Length);
		}
		lastStep = rand;
		audioSource.pitch = Random.Range(pitch.x, pitch.y);
		audioSource.PlayOneShot(footsteps[rand]);
	}
}
