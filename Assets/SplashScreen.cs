﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour
{
	bool loading = false;
	void Update()
	{
		if (!loading) {
			if (Input.GetMouseButtonDown(0)) {
				loading = true;
				FadeIt();
				Application.LoadLevelAsync(1);
			}
		}

		//whitePix.color = new Color(0, 0, 0, alpha);

	}

	public SpriteRenderer loadingScreen;
	public float loadingFadeDur;


	public SpriteRenderer whitePix;
	public float alpha;
	public float dur;

	public AnimationCurve fadeCurve;


	void FadeIt()
	{
		StartCoroutine(pTween.To(loadingFadeDur, tt => {
			loadingScreen.color = new Color(1, 1, 1, fadeCurve.Evaluate(tt));
		}));
	}
}
