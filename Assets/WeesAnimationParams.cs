﻿using UnityEngine;
using System.Collections;

public class WeesAnimationParams : MonoBehaviour
{

	Animator anim;
	public float speedMultiplier = 1;

	void Start()
	{
		anim = GetComponent<Animator>();
	}

	void Update()
	{

		var nav = GetComponent<NavMeshAgent>();
		var speed = nav.velocity.magnitude * speedMultiplier;
		anim.SetFloat("Speed", speed);
	}
}
