﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TileShit : MonoBehaviour {

	public int x, y;
	public Transform t;
	public bool doit;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (doit) {
			doit = false;
			for (int i = 0; i < x; i++) {
				for (int j = 0; j < y; j++) {
					var pos = new Vector3(i * t.localScale.x, t.position.y, j*t.localScale.z) + transform.position;
					var inst = Instantiate(t, pos, t.rotation) as Transform;
					inst.parent = transform;
				}
			}
		}
	}
}
