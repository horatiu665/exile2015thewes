﻿using UnityEngine;
using System.Collections;

public class AnimatorRootMotionToNavMeshAgent : MonoBehaviour {

	private Animator anim;
	private NavMeshAgent agent;

	void Start()
	{
		anim = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
	}


	void OnAnimatorMove()
	{
		// get animator velocity data
		var agentVelocity = anim.deltaPosition / Time.deltaTime;
		// set agent velocity on navmeshagent
		agent.velocity = agentVelocity;
	}
}
