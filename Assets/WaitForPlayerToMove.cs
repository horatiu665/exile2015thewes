﻿using UnityEngine;
using System.Collections;
using PlantmanAI4;


public class WaitForPlayerToMove : MonoBehaviour, IState
{

	public string GetName()
	{
		return "wait for player to move";
	}

	public bool GetUninterruptible()
	{
		return false;
	}
	public float priority;
	public float GetPriority()
	{
		return priority;
	}

	public void OnEnter()
	{

	}

	public void OnExecute(float deltaTime)
	{
	}

	public void OnExit()
	{
		
	}

	public bool ConditionsMet()
	{
		return Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0;
	}
}
