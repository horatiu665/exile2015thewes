﻿using UnityEngine;
using System.Collections;

public class WinCondition : MonoBehaviour {

	public static WinCondition inst;

	public LayerMask stuckLayer;
	public float offset;

	public initFadeFromBlack fadeToWhite;

	// Use this for initialization
	void Start () {
		inst = this;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawLine(transform.position, transform.position + Vector3.up * 100, CheckForLose() ? Color.red : Color.green);
		if (CheckForLose()) {
			LoseGame();
		}
	}


	public void LoseGame()
	{
		Destroy(GetComponent<CapsuleCollider>());
		GetComponent<CharacterController>().enabled = false;
		// fade to white
		fadeToWhite.FadeIt();
		StartCoroutine(pTween.To(3, t => {
			if (t == 1) {
				Application.LoadLevel(Application.loadedLevel);
			}
		}));
	}

	public int numRaycasts;
	public float range;

	bool CheckForLose()
	{
		
		for (int i = 0; i < numRaycasts; i++) {
			var angle = Mathf.Lerp(0, 360, i / (float)(numRaycasts - 1)) + offset;
			var dir = new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
			var hitSomething = Physics.Raycast(transform.position, dir, range, stuckLayer);
			
			Debug.DrawLine(transform.position, transform.position + dir * range);
			if (!hitSomething) {
				return false;
			}
		}
		return true;
	}
}
